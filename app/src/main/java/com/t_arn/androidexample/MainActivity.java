package com.t_arn.androidexample;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private final String TAG = "MainActivity";
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate() start");
        super.onCreate(savedInstanceState);
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);
        // add your stuff here
        Button button = new Button(this);
        button.setText("Click me!");
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonClick();
            }
        };
        button.setOnClickListener(listener);
        layout.addView(button);
        this.textView = new TextView(this);
        this.textView.setText("Hello World!");
        layout.addView(this.textView);
        // set the main content
        this.setContentView(layout);
        Log.d(TAG, "onCreate() complete");
    }

    public void onButtonClick() {
        Log.d(TAG, "onButtonClick() start");
        this.textView.append("\nButton clicked");
        Log.d(TAG, "onButtonClick() stop");
    }

    protected void onStart() {
        Log.d(TAG, "onStart() start");
        super.onStart();
        Log.d(TAG, "onStart() complete");
    }

    protected void onResume() {
        Log.d(TAG, "onResume() start");
        super.onResume();
        Log.d(TAG, "onResume() complete");
    }

}